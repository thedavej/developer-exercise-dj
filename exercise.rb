class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    # TODO: Implement this method
    
    result = []
    str.split.each {|w|
    if w.length > 4 then
      last_char = w[-1, 1]
      w == w.capitalize ? w = "Marklar" : w = "marklar"
      last_char =~ /\p{P}/ ? w << last_char : nil
    end
    result << w
    }
    return result.join(' ')
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    # TODO: Implement this method
    a = 0
    b = 1
    array = []
    count = 0
    sum = 0

    # Generate Fibonacci sequence to nth degree
    while count < nth do
      array << b
      a,b = b,a+b
      count += 1
    end
    
    # Calculate sum of even numbers only
    array.each {|num| 
      num %2 == 0 ? sum += num : false}
    return sum
  end
end
